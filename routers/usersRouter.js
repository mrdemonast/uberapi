const Router = require('express');
const router = new Router();
const UsersController = require('../controllers/UsersController');
const authMiddleware = require('../middlewares/authMiddleware');

router.get('/me', authMiddleware, UsersController.getProfileInfo);
router.delete('/me', authMiddleware, UsersController.deleteProfile);
router.patch('/me/password', authMiddleware, UsersController.changePassword);

module.exports = router;
