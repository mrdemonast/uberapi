require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');
const router = require('./routers/allRoutes');

const app = express();
app.use(morgan('tiny'));
app.use(express.json());
app.use(cors());
app.use(router);

const PORT = +process.env.PORT || 8080;
const DB = process.env.DB;

const startServer = async () => {
  await mongoose.connect(DB, {useNewUrlParser: true});
  app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
};
startServer();
